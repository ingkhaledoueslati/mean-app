const express = require('express');
const router = express.Router();
const Recipe = require('../models/recipe');
const recipeCtrl = require('../controllers/recipe');

// new recipe
router.post('/', recipeCtrl.createRecipe);
// return a specific one
router.get('/:id', recipeCtrl.getOneRecipe);
// returning all recipes
router.get('/',recipeCtrl.getAllRecipe );
//update
router.put('/:id', recipeCtrl.modifyRecipe);
// delete 
router.delete('/:id', recipeCtrl.deleteRecipe);

module.exports = router;